﻿using CurriculoAPI.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace CurriculoAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            // url base da API
            string url = "http://localhost/webservice/curriculo.json";

            // consumindo API
            Curriculo Curriculo = BuscarCurriculo(url);

            Console.WriteLine("\n===================Informações do Curriculo==================");

            // exibindo dados retornados
            Console.WriteLine(string.Format("\nNome: {0} \nEmail: {1} - \nData de Nascimento: {2} ", Curriculo.Nome, Curriculo.Email, Curriculo.Datanascimento ));

            Console.WriteLine("\nEndereço:");
            Console.WriteLine(string.Format("\nRua: {0}  -  N°: {1} \nBairro: {2}  -  Cidade: {3} \nEstado: {4}  -  Pais:{5}", Curriculo.Endereco.Rua,Curriculo.Endereco.Numero, Curriculo.Endereco.Bairro, Curriculo.Endereco.Cidade
                , Curriculo.Endereco.Estado, Curriculo.Endereco.Pais));

            Console.WriteLine("\nProgramas:\n");

            foreach (string pro in Curriculo.Programas)
            {
                Console.WriteLine(pro);
            }
            Console.WriteLine("\nFormação:");

            foreach (Formacao f in Curriculo.Formacao)
            {
                Console.WriteLine(string.Format("\nEscolaridade: {0} \nCurso: {1} \nInstituição: {2} \nConclusão: {3} ", f.Escolaridade,f.Curso,f.Instituicao,f.Conclusao));
            }


            // matendo o console aberto
            Console.ReadLine();
        }
        // função destinada consumir a API que retornará os dados de um veiculo
        public static Curriculo BuscarCurriculo(string url)
        {
            //Instanciando WebCliente para chamdas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            //Console.WriteLine(content);
            //Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Curriculo Veiculo = JsonConvert.DeserializeObject<Curriculo>(content);

            // retornando computador
            return Veiculo;

        }
    }
}
