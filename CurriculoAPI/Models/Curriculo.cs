﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurriculoAPI.Models
{
    class Curriculo
    {    
            public string Nome { get; set; }
            public string Datanascimento { get; set; }
            public string Email { get; set; }
            public Endereco Endereco { get; set; }
            public List<Formacao> Formacao { get; set; }
            public List<string> Programas { get; set; }
    }
    class Endereco
        {
            public string Rua { get; set; }
            public string Numero { get; set; }
            public string Bairro { get; set; }
            public string Cidade { get; set; }
            public string Estado { get; set; }
            public string Pais { get; set; }
        }

    class Formacao
        {
            public string Escolaridade { get; set; }
            public string Curso { get; set; }
            public string Instituicao { get; set; }
            public string Conclusao { get; set; }
        }
}
