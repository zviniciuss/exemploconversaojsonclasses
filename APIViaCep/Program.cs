﻿using APIViaCep.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace APIViaCep
{
    class Program
    {
        static void Main(string[] args)
        {
            string cep;
            Console.WriteLine("Digite o CEP que deseja buscar: ");
            cep = Console.ReadLine();
            string url = "https://viacep.com.br/ws/" + cep + "/json/";
            ViaCep viaCep = BuscarCep(url);
            Console.WriteLine("\n==============================================================");
            Console.WriteLine("==============================================================");
            Console.WriteLine(string.Format("\nCEP: {0} \nLogradouro: {1} \nComplemento: {2} \nBairro: {3} \nLocalidade: {4} \nUF: {5} \n" +
                "Unidade: {6} \nIBGE: {7} \nGIA: {8}", viaCep.Cep, viaCep.Logradouro, viaCep.Complemento, viaCep.Bairro, viaCep.Localidade
                ,viaCep.Uf,viaCep.Unidade, viaCep.Ibge, viaCep.Gia));
            Console.WriteLine("\n==============================================================");
            Console.WriteLine("==============================================================");

            Console.ReadLine();
        }
        public static ViaCep BuscarCep(string url)
        {
            //Instanciando WebCliente para chamdas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            //Console.WriteLine(content);
            //Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            ViaCep User = JsonConvert.DeserializeObject<ViaCep>(content);

            // retornando computador
            return User;

        }
    }
}
