﻿using System;
using System.Net;
using DesafioAPIUsuario.Models;
using Newtonsoft.Json;

namespace DesafioAPIUsuario
{
    class Program
    {

        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/usuario";

            // consumindo API
            Usuario Usuario = BuscarUsuario(url);

            Console.WriteLine("\nInformações:");

            // exibindo dados retornados
            Console.WriteLine(string.Format("\nNome: {0} - Email: {1} - Telefone: {2} ", Usuario.Nome, Usuario.Email, Usuario.Telefone));

            Console.WriteLine("\nOpcionais:");

            foreach (string c in Usuario.Conhecimentos)
            {
                Console.WriteLine("- " + c);
            }

            Console.WriteLine("\nEndereço:");

            Console.WriteLine(string.Format("\nRua: {0} \nNumero: {1} \nBairro: {2} \nCidade: {3} \n" +
                "UF: {4}", Usuario.Endereco.Rua, Usuario.Endereco.Numero, Usuario.Endereco.Bairro, Usuario.Endereco.Cidade, Usuario.Endereco.UF));

            Console.WriteLine("\nQualificacões:");

            foreach (Qualificacao q in Usuario.Qualificacoes)
            {
                Console.WriteLine(string.Format("\nNome: {0} \nInstituição: {1} \nAno: {2} ", q.Nome, q.Instituicao, q.Ano));
            }


            // matendo o console aberto
            Console.ReadLine();
        }
        // função destinada consumir a API que retornará os dados de um veiculo
        public static Usuario BuscarUsuario(string url)
        {
            //Instanciando WebCliente para chamdas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            //Console.WriteLine(content);
            //Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Usuario Veiculo = JsonConvert.DeserializeObject<Usuario>(content);

            // retornando computador
            return Veiculo;
           
        }
    }
}
