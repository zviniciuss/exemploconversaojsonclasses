﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace ExemploConversaoJsonClasses.Models
{
    class Veiculo
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Ano { get; set; }
        public int Quilometragem { get; set; }
        public List<string> Opcionais { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Revisao> Revisoes { get; set; }
    }
    class Vendedor
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string Celular { get; set; }
        public string Cidade { get; set; }
    }
    class Revisao
    {
        public DateTime Data { get; set; }
        public int KM { get; set; }
        public string Oficina { get; set; }
    }
}
