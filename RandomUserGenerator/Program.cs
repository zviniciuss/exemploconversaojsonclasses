﻿using Newtonsoft.Json;
using RandomUserGenerator.Models;
using System;
using System.Net;

namespace RandomUserGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            int nu;
            string nacionalidade, nacio;
            int res;
            string url = "https://randomuser.me/api/";
            Console.WriteLine("Quantos Usuarios deseja buscar ?");
            nu = int.Parse(Console.ReadLine());        
            Console.WriteLine("Qual a Nacionalidade deseja buscar ?"); 
            nacionalidade = Console.ReadLine();
            Console.WriteLine("Deseja adicionar outra nacionalidade ? ");
            Console.WriteLine("\nDigite 1 para SIM ou 2 para NAO");
            res = int.Parse(Console.ReadLine());

            switch (res)
            {
                case 1:
                    Console.WriteLine("Qual outra nacionalidade deseja buscar ? ");
                    nacio = Console.ReadLine();
                    url = url + "?results=" + nu + "&nat=" + nacionalidade + "," + nacio;
                    break;
                default:
                    url = url + "?results=" + nu + "&nat=" + nacionalidade;
                    break;
            }

            RandomUser RandomUser = BuscarUser(url);
            foreach(InformacoesUsuario iu in RandomUser.Results)
            {
                Console.WriteLine("\n==========================================================================================");  //12071-670
                Console.WriteLine("Informações de(a) " + iu.Name.First + ":");
                Console.WriteLine("\nNome Completo: " + iu.Name.Title + " " + iu.Name.First + " " + iu.Name.Last);
                Console.WriteLine(string.Format("Genero: {0}  -  Email: {1}", iu.Gender, iu.Email));
                Console.WriteLine("------------------------------------------------------------------------------------------");
                Console.WriteLine("Localização:");
                Console.WriteLine(string.Format("\nRua: {0}  -  N°: {1} \nCidade: {2}  -  Estado: {3}  -  Pais: {4}  \nCEP: {5}", iu.Location.Street.Name, 
                    iu.Location.Street.Number ,iu.Location.City, iu.Location.State, iu.Location.Country,iu.Location.Postcode));
                Console.WriteLine("------------------------------------------------------------------------------------------");
                Console.WriteLine("Coordenadas:");
                Console.WriteLine(string.Format("\nX: {0} \nY: {1}", iu.Location.Coordinates.Latitude, iu.Location.Coordinates.Longitude));
                Console.WriteLine("------------------------------------------------------------------------------------------");
                Console.WriteLine("Fuso Horário:");
                Console.WriteLine(string.Format("\nHorário: {0}  -  Descrição: {1}", iu.Location.Timezone.Offset, iu.Location.Timezone.Description));
                Console.WriteLine("==========================================================================================");

            }
            Console.ReadLine();
        }
        public static RandomUser BuscarUser(string url)
        {
            //Instanciando WebCliente para chamdas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            //Console.WriteLine(content);
            //Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            RandomUser User = JsonConvert.DeserializeObject<RandomUser>(content);

            // retornando computador
            return User;

        }
    }
}
